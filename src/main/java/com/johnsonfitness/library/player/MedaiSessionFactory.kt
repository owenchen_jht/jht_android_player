package com.johnsonfitness.library.player

import android.content.Context
import com.johnsonfitness.library.player.media.AbstractMediaSession
import com.johnsonfitness.library.player.media.ExoPlayerMediaSession
import com.johnsonfitness.library.player.media.IjkPlayerMediaSession

enum class TYPE { EXOPLAYER, IJKPLAYER }

object MediaSessionFactory {


    fun createMediaSession(context: Context, type: TYPE, source: Source): AbstractMediaSession {
        return when (type) {
            TYPE.EXOPLAYER -> {
               ExoPlayerMediaSession(context, source)
            }
            TYPE.IJKPLAYER ->{
                IjkPlayerMediaSession(source)
            }
            else -> {
                throw IllegalArgumentException("not implement it type: $type")
            }
        }
    }
}





