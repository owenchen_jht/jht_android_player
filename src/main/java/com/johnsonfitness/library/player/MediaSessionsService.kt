package com.johnsonfitness.library.player

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.SparseArray
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.util.forEach
import com.johnsonfitness.library.player.media.AbstractMediaSession
import com.johnsonfitness.library.player.media.OnStateListener
import java.lang.IllegalArgumentException
import android.app.NotificationManager

import android.app.NotificationChannel
import android.os.Build


class MediaSessionsService : Service(), OnStateListener {


    private val mediaSessionMap = SparseArray<MediaSource>()

    private val binder = MediaSessionsBinder()

    override fun onCreate() {
        super.onCreate()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val CHANNEL_ID = "my_channel_01"
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService (Context.NOTIFICATION_SERVICE) as NotificationManager)
                .createNotificationChannel(channel)

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("service is running...")
                .setContentText("run player service")
                .build()
            startForeground(1, notification)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val videoId = intent?.getStringExtra(EXTRA_VIDEO_ID)
            ?: return super.onStartCommand(intent, flags, startId)

        when (intent.action) {
            ACTION_PLAY -> {
                getMediaSession(videoId)?.start()
            }
            ACTION_PAUSE -> {
                getMediaSession(videoId)?.pause()
            }
            ACTION_STOP -> {
                getMediaSession(videoId)?.stop()
            }
            ACTION_RELEASE -> {
                getMediaSession(videoId)?.release()
            }
            else -> {
                throw IllegalArgumentException("${intent.action} is illegal action parameters")
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onBind(intent: Intent): IBinder = binder

    override fun onDestroy() {
        mediaSessionMap.forEach { _, mediaSource ->
            mediaSource.mediaSession.release()
        }
        mediaSessionMap.clear()
    }

    override fun onPreparing(mediaSession: AbstractMediaSession) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onPreparing(mediaSession.getVideoId())
    }

    override fun onPrepared(mediaSession: AbstractMediaSession) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onPrepared(mediaSession.getVideoId())
    }

    override fun onPaused(mediaSession: AbstractMediaSession) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onPaused(mediaSession.getVideoId())
    }

    override fun onStop(mediaSession: AbstractMediaSession) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onStop(mediaSession.getVideoId())
    }

    override fun onCompleted(mediaSession: AbstractMediaSession) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onCompleted(mediaSession.getVideoId())
    }

    override fun onError(mediaSession: AbstractMediaSession, error: Any?) {
        mediaSessionMap.get(mediaSession.getVideoId().hashCode())?.listener?.onError(mediaSession.getVideoId())
    }

    fun addMediaSession(mediaSession: AbstractMediaSession, listener: OnMediaStateListener) {
        mediaSessionMap.put(
            mediaSession.getVideoId().hashCode(),
            MediaSource(mediaSession.apply { stateListener = this@MediaSessionsService }, listener)
        )
    }

    fun removeMediaSession(videoId: String) {
        mediaSessionMap.remove(videoId.hashCode())
    }

    fun getMediaSession(videoId: String): AbstractMediaSession? {
        return mediaSessionMap.get(videoId.hashCode())?.mediaSession
    }


    inner class MediaSessionsBinder : Binder() {
        fun getService() = this@MediaSessionsService
    }


    interface OnMediaStateListener {

        fun onPreparing(videoId: String)

        fun onPrepared(videoId: String)

        fun onPaused(videoId: String)

        fun onStop(videoId: String)

        fun onCompleted(videoId: String)

        fun onError(videoId: String)
    }


    private inner class MediaSource(
        val mediaSession: AbstractMediaSession,
        val listener: OnMediaStateListener
    )


    companion object {

        const val EXTRA_VIDEO_ID = "extra_video_id"

        const val ACTION_PLAY = "extra_action_play"
        const val ACTION_PAUSE = "extra_action_pause"
        const val ACTION_STOP = "extra_action_stop"
        const val ACTION_RELEASE = "extra_action_release"

        fun getIntent(context: Context) = Intent(context, MediaSessionsService::class.java)

        fun startService(context: Context, videoId: String? = null, action: String? = null) {
            ContextCompat.startForegroundService(context, getIntent(context).apply {
                putExtra(EXTRA_VIDEO_ID, videoId)
                this.action = action
            })
        }
    }
}