package com.johnsonfitness.library.player

import android.os.Parcel
import android.os.Parcelable

data class Source(
    val videoId: String,
    val videoPath: String,
    val seekTime: Long,
    val isLive: Boolean = false
): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(videoId)
        parcel.writeString(videoPath)
        parcel.writeLong(seekTime)
        parcel.writeByte(if (isLive) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Source> {
        override fun createFromParcel(parcel: Parcel): Source {
            return Source(parcel)
        }

        override fun newArray(size: Int): Array<Source?> {
            return arrayOfNulls(size)
        }
    }
}
