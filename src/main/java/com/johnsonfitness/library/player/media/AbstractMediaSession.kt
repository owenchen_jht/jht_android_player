package com.johnsonfitness.library.player.media

import android.view.Surface

abstract class AbstractMediaSession : IMediaPlayerAction {

    var stateListener: OnStateListener? = null

    var surface: Surface? = null

    abstract fun getVideoId(): String

    abstract fun setPlayerSurface(_surface: Surface)

    abstract fun prepare()

    abstract fun start()

    abstract fun stop()

    abstract fun pause()

    abstract fun release()

    abstract fun isPlaying(): Boolean

    abstract fun retry()

    // ====== IMediaPlayerAction =====

    override fun seekTo(position: Long) {}

    override fun mute(mute: Boolean) {}

    // ====== IMediaPlayerAction End =====
}


interface IMediaPlayerAction {

    fun seekTo(position: Long)

    fun mute(mute: Boolean)
}