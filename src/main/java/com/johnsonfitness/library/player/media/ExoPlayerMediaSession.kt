package com.johnsonfitness.library.player.media

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.Surface
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.ext.rtmp.RtmpDataSource
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSource
import com.google.android.exoplayer2.util.Util
import com.johnsonfitness.library.player.Source


class ExoPlayerMediaSession(context: Context, private var source: Source) : AbstractMediaSession() {


    private val player: Player = ExoPlayer.Builder(context.applicationContext).build()

    private val dataSourceFactory = DefaultDataSource.Factory(context)

    private val playerListener = object : Player.Listener {

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            Log.d(TAG, "onPlayerStateChanged: $playWhenReady $playbackState")
            if (!playWhenReady) {
                stateListener?.onPaused(this@ExoPlayerMediaSession)
                return
            }

            when (playbackState) {
                Player.STATE_READY -> {
                    stateListener?.onPrepared(this@ExoPlayerMediaSession)
                }
                Player.STATE_BUFFERING -> {
                    stateListener?.onPreparing(this@ExoPlayerMediaSession)
                }
                Player.STATE_ENDED -> {
                    stateListener?.onCompleted(this@ExoPlayerMediaSession)
                }
            }
        }

        override fun onPlayerError(error: PlaybackException) {
            Log.d(TAG, "onPlayerError: ${error.message}")
            // TODO do retry or not
            stateListener?.onError(this@ExoPlayerMediaSession, error.message)
        }
    }

    private fun createMediaSource(source: Source): MediaSource {
        Log.d(TAG, "createMediaSource: source: ${source.videoPath}")
        val url = Uri.parse(source.videoPath)
        val mediaItem = MediaItem.fromUri(source.videoPath)
        return when (Util.inferContentType(url)) {
            C.TYPE_DASH -> {
                Log.d(TAG, "createMediaSource: TYPE_DASH")
                DashMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
            }
            C.TYPE_HLS -> {
                Log.d(TAG, "createMediaSource: TYPE_HLS")
                HlsMediaSource.Factory(dataSourceFactory)
                    .createMediaSource(mediaItem)
            }
            else -> {
                Log.d(TAG, "createMediaSource: other")
                val sourceFactory = if ("rtmp" == url.scheme) RtmpDataSource.Factory() else dataSourceFactory
                ProgressiveMediaSource.Factory(sourceFactory)
                    .createMediaSource(mediaItem)
            }

        }
    }

    private fun setupMediaSource() {
        val source = ConcatenatingMediaSource(createMediaSource(source))
        (player as ExoPlayer).addMediaSource(source)
    }

    override fun getVideoId(): String = source.videoId

    override fun setPlayerSurface(_surface: Surface) {
        this.surface = _surface
        player.setVideoSurface(surface)
    }

    override fun prepare() {
        stateListener?.onPreparing(this)
        setupMediaSource()

        with(player) {
            setPlayerSurface(surface!!)
            addListener(playerListener)
            prepare()
        }
    }

    override fun start() {
        player.playWhenReady = true
    }

    override fun pause() {
        player.playWhenReady = false
    }

    override fun stop() {
        player.stop()
    }

    override fun isPlaying(): Boolean = player.isPlaying

    override fun retry() {
        //TODO do retry logic
    }

    override fun release() {
        player.release()
    }


    companion object {
        val TAG: String = ExoPlayerMediaSession::class.java.simpleName
    }
}