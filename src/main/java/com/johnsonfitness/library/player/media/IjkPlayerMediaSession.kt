package com.johnsonfitness.library.player.media

import android.view.Surface
import com.johnsonfitness.library.player.Source
import tv.danmaku.ijk.media.player.IMediaPlayer
import tv.danmaku.ijk.media.player.IjkMediaPlayer

class IjkPlayerMediaSession(private var source: Source) : AbstractMediaSession() {

    private val player = IjkMediaPlayer()

    var isPrepared = false

    private val onPreparedListener = IMediaPlayer.OnPreparedListener {
        it.start()
        isPrepared = true
        stateListener?.onPrepared(this@IjkPlayerMediaSession)
    }

    private val onCompletedListener = IMediaPlayer.OnCompletionListener {
        isPrepared = false
        stateListener?.onCompleted(this@IjkPlayerMediaSession)
    }

    private val onErrorListener = IMediaPlayer.OnErrorListener { mp, what, extra ->
        isPrepared = false
        stateListener?.onError(this@IjkPlayerMediaSession, what)
        false
    }

    init {
        with(player) {
            dataSource = source.videoPath

            //  設定播放前的最大探測時間
            setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "analyzemaxduration", 1000000)
            //  設定播放前的探測時間(達到首屏秒開效果 (microsecond))
            setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "analyzeduration", 1000000)
            //  播放前的探測大小，預設值為1MB，改小一點出畫面會更快
            setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "probesize", 4096)
            //  每處理一個packet後更新io上下文
            setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "flush_packets", 1L)

            //  設置是否開啟環路過濾：0開啟，畫質高，解碼loading大；48關閉，畫質較低，解碼loading小
            setOption(IjkMediaPlayer.OPT_CATEGORY_CODEC, "skip_loop_filter", 0)
            //  跳幀處理。當CPU處理較慢時，進行跳幀處理，保證播放時，畫面與聲音同步
            setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "framedrop", 5)
            //  最大Buffer大小，單位 KB
            setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "max-buffer-size", MAX_CACHE_SIZE)
            //  最大fps
            setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "max-fps", 30)
            //  優化 SeekTo
            setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "enable-accurate-seek", 1)
            //  Drop bad frame
            setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "err_detect", "explode")

            if (source.isLive) {
                // Param for living
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "max_cached_duration", 3000)
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "infbuf", 1)
                //  是否開啟 pre loading。一般直播項目會開啟，達到秒開效果。不過可能會有播放丟幀卡頓的狀況
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "packet-buffering", 0)
            } else {
                // Param for playback
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "max_cached_duration", 0)
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "infbuf", 0)
                setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "packet-buffering", 1)
            }
            setVolume(1.0f, 1.0f)

            setOnPreparedListener(onPreparedListener)
            setOnCompletionListener(onCompletedListener)
            setOnErrorListener(onErrorListener)
        }
    }

    override fun getVideoId(): String =  source.videoId

    override fun setPlayerSurface(_surface: Surface) {
        surface = _surface
        player.setSurface(surface)
    }

    override fun prepare() {
        player.prepareAsync()
        stateListener?.onPreparing(this)
    }

    override fun start() {
        if (!player.isPlaying) {
            player.start()
        }
    }

    override fun stop() {
        if (player.isPlaying) {
            player.stop()
            stateListener?.onStop(this)
        }
    }

    override fun pause() {
        if (player.isPlaying) {
            player.pause()
            stateListener?.onPaused(this)
        }
    }

    override fun release() {
        if (player.isPlaying) {
            player.stop()
        }
        player.release()
    }

    override fun isPlaying(): Boolean {
        return player.isPlaying
    }

    override fun retry() {
        stop()
        start()
    }


    companion object {
        private const val MAX_CACHE_SIZE: Long = 512 * 1024
    }
}