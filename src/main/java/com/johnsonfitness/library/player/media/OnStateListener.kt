package com.johnsonfitness.library.player.media

interface OnStateListener {

    fun onPreparing(mediaSession: AbstractMediaSession)

    fun onPrepared(mediaSession: AbstractMediaSession)

    fun onPaused(mediaSession: AbstractMediaSession)

    fun onStop(mediaSession: AbstractMediaSession)

    fun onCompleted(mediaSession: AbstractMediaSession)

    fun onError(mediaSession: AbstractMediaSession, error: Any?)
}